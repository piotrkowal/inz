data = [10, 3, 7, 8, 1, 6, 2, 9, 4, 5, 0]

def bubble_sort(seq):
    changed = True
    while changed:
        changed = False
        for i in range(len(seq) - 1):
            if seq[i] > seq[i+1]:
                seq[i], seq[i+1] = seq[i+1], seq[i]
                changed = True
    return seq

bubble_sort(data)
print(data)
