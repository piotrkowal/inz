import os
import subprocess
import matplotlib
import locale
import numpy as np 
from random import shuffle
from sys import exit
matplotlib.use('Agg')
locale.setlocale(locale.LC_ALL, 'pl_PL.UTF-8')

import matplotlib.pyplot as plot

nameStandards = {'python3':'.py','c':'.out'}

class Runner:
    data = {}
    allowedArguments = []
    arguments = []
    finalData = {}

    def __init__(self, name, path):
        self.data['name'] = name
        self.data['programs'] = {}
        for key, value in nameStandards.items():

            exists = os.path.exists(path+self.data['name']+'/'+self.data['name']+value)
            if not exists:
                continue

            self.data['programs'].update({key:path+name+'/'+name+value})

        try:
            with open(path+self.data['name']+'/'+'config.dat') as file:
                config = file.read()
                config = config.split('\n')
                for conf in config:
                    if conf:
                        conf = conf.split(' ')
                        self.data.update({conf[0]:conf[1]})
        except FileNotFoundError:
            print(" Nie znaleziono pliku 'config.dat' !")
            self.data['delay'] = 0

        self.configRunner(path)
        self.data['measurements'] = {} 
        for key in self.data['programs'].keys():
            self.data['measurements'].update({key:{}})
            self.finalData.update({key:{}})
            for value in self.allowedArguments:
                self.finalData[key].update({value:{'uss':0,'pss':0,'rss':0,'cpu':0,'time':0.0,}})
                self.data['measurements'][key].update({
                            value:{
                                'uss':[],
                                'pss':[],
                                'rss':[],
                                'cpu':[],
                                'time':[],
                             }
                })
        self.arguments = 2 * self.allowedArguments
        shuffle(self.arguments)
    
    def configRunner(self, path):
        text = ''
        try:
            with open(path+self.data['name']+'/allowedArgs.dat') as file:
                text = file.read()
        except FileNotFoundError:
            print("Nie znaleziono pliku 'allowedArgs.dat' w folderze",path+self.data['name'])
            exit()
        
        text = text.replace('\n','').split(' ')
        args = []
        for i in text:
            if i:
                args.append(int(i))
        self.allowedArguments = text

    def getMem(self, cmd):
        print(('# Przeprowadzam test pamięci - '+cmd).encode('utf-8'))
        os.system(cmd + '&')
        mem = str(subprocess.check_output(['smem']))
        mem = mem.split('\\n')
        for i in mem:
            print(i)

        print(mem)
        print(self.data['name'])
        mem = [s for s in mem if self.data['name'] in s]        
        memTemp = []
        import time
        time.sleep(10)
        for i in mem[0].split(' '):
            if i:
                memTemp.append(i)
        

        pid = memTemp[0]
        os.system('kill '+pid+'&')
        return {
            'uss':int(memTemp[-3]),
            'pss':int(memTemp[-2]),
            'rss':int(memTemp[-1])
        }
    
    #measure cpuTime
    def getCpuTime(self,cmd=''):
        print('# Przeprowadzam test CPU i czasu - '+cmd)
        print('perf stat -o cpu '+cmd+ '> /dev/null')
        os.system('perf stat -o cpu '+cmd+ ' > /dev/null')
        output = []
        with open('cpu') as file:
            output = file.read()     
        output = output.split('\n')
        print(output)
        cpu = output[9].split(' ')
        cpu = list(filter(None, cpu))
        print(cpu)
        time = output[-3].split(' ')
        print(time)
        time = list(filter(None, time)) # fastest
        #print(time)
        return { 
            'cpu':int(cpu[0]), 
            'time':float(locale.atof(time[0]))
        }                


    def runTest(self):
        for key, value in self.data['programs'].items():
            if key is not 'c':
                for argument in self.arguments:
                    mem = self.getMem(key+' '+value+' '+str(argument))
                    cpuTime = self.getCpuTime(str(key+' '+value+' '+str(argument)))
                    mem.update(cpuTime) 
                    for name, size in mem.items():
                        self.data['measurements'][key][argument][name].append(size)
            elif key is 'c':
                for argument in self.arguments:
                    mem = self.getMem(self.data['programs'][key]+' '+str(argument))
                    cpuTime = self.getCpuTime(self.data['programs'][key]+' '+str(argument))
                    mem.update(cpuTime)
                    for name, size in mem.items():
                        self.data['measurements'][key][argument][name].append(size)
            else:
                for argument in self.arguments:
                    mem = self.getMem('./'+value+' '+str(argument))
                    cpuTime = self.getCpuTime(str('./'+value+' '+str(argument)))
                    mem.update(cpuTime) 
                    for name, size in mem.items():
                        self.data['measurements'][key][argument][name].append(size)
        self.calculateMeanAverage() 

    def calculateMeanAverage(self):
        data = self.data['measurements']
        for key in self.data['programs'].keys():
            for arg in self.allowedArguments:
                
                temp_cpu =  np.mean(data[key][arg]['cpu'])
                temp_time =  np.mean(data[key][arg]['time'])
                temp_rss =  np.mean(data[key][arg]['rss'])
                temp_pss =  np.mean(data[key][arg]['pss'])
                temp_uss =  np.mean(data[key][arg]['uss'])
                temp_time = np.mean(temp_time) - int(self.data['delay'])
                self.finalData[key][arg]['time'] = temp_time

                self.finalData[key][arg]['cpu'] = temp_cpu
                self.finalData[key][arg]['rss'] = temp_rss
                self.finalData[key][arg]['uss'] = temp_uss
                self.finalData[key][arg]['pss'] = temp_pss


    def getFinalData(self):
        return self.finalData
    

