from runner import Runner
from ploter import Ploter
from subprocess import check_output
import sys
import json
class App:
    alg = []
    path = 'al/'
    runners = {}
    ploter = Ploter()

    def getAlgorythms(self):
        temp = str(check_output(['ls', self.path]))
        self.alg = temp.replace("b'", '').split('\\n')
        self.alg.pop()

    def run(self, num=0,exportFileName='export'):
        self.getAlgorythms()

        for i in range(len(self.alg)):
            print(i, ' - ', self.alg[i])
        
        what=num
        self.runners.update({
            self.alg[what]: Runner(self.alg[what], self.path)
        })

        for name in self.runners.keys():
            self.runners[name].runTest()
            with open('/home/pi/exports/'+exportFileName+'.json', 'a+') as file:
                json.dump(self.runners[name].getFinalData(),file)
            
            self.ploter.plot(
                    name, 
                    self.runners[name].getFinalData(),
                    self.runners[name].allowedArguments
            )


if __name__ == '__main__':
    print(sys.argv[1])
    if sys.argv[1] and sys.argv[2]:
        app = App()
        app.run(int(sys.argv[1]),str(sys.argv[2]))
    app = App()
    app.run()
