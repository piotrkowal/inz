import matplotlib
import numpy as np 
matplotlib.use('Agg')
import matplotlib.pyplot as plot

class Ploter:
    def plot(self,name, data, args):
        
        dataToPlot = {}     
        for key, value in data.items():
            dataToPlot.update({key:{'rss':[],'pss':[],'uss':[],'cpu':[],'time':[],}})
        for key, value in data.items():
             
            temp = value
            args=sorted(temp)
            counter = 0
            for arg in args:
                dataToPlot[key]['rss'].append(temp[arg]['rss'])
                dataToPlot[key]['pss'].append(temp[arg]['pss'])
                dataToPlot[key]['uss'].append(temp[arg]['uss'])
                dataToPlot[key]['time'].append(temp[arg]['time'])
                dataToPlot[key]['cpu'].append(temp[arg]['cpu'])
            args = list(map(int, args))

        for key, value in dataToPlot.items():
            for size in value:
                if value[size]:
                    figure = plot.figure(counter)
                    axis = figure.add_subplot(111)
                    figure.suptitle(size+' vs imput size')
                    axis.set_title(key)
                    axis.set_ylabel(size)
                    axis.set_xlabel('rozmiar wejscia')
                    args,value[size] = zip(*sorted(zip(args, value[size])))                               
                    axis.plot(args, value[size])
                    plot.savefig(name+'_'+key+'_'+size)
                    counter+=1
                    del figure
