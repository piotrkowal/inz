#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main(int argc, char **argv)
{
	int* list;
	int size;
	int i,j,temp;

	sscanf(argv[1],"%i", &size);

	list = (int*) calloc(size, sizeof(int));
        srand(time(NULL));

	for(i=0; i<size; i++) {
		list[i] = rand()%size;
	}
        
        for(i =0; i<size; i++) {
            for(j=0; j<size -1; j++) {
                if(list[j] > list[j+1]) {
                    temp = list[j];
                    list[j] = list[j+1];
                    list[j+1] = temp;
                }
            }
        }
        sleep(5);
	return(0);
}
