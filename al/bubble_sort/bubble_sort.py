import sys
from time import sleep
def bubble_sort(arg):
    package = 'arg'
    a = getattr(__import__(package, fromlist=['a'+str(arg)]),'a'+str(arg) )
    changed = True
    while changed:
        changed = False
        for i in range(len(a) - 1):
            if a[i] > a[i+1]:
                a[i], a[i+1] = a[i+1], a[i]
                changed = True
    return a

lists = bubble_sort(int(sys.argv[1]))
sleep(5)
