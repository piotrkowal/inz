#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fftw3.h>

int main(int argc, char **argv)
{
//    fftw_complex *in;
  //  fftw_complex *out;

    int size;
    int i,j,temp;
  //  fftw_plan plan_forward; 

    sscanf(argv[1],"%i", &size);
    
    fftw_complex *in;
    int n = 7;
    fftw_complex *out;
    fftw_plan plan_backward;
    fftw_plan plan_forward;
    //fftw_complex in[] = {{0,0},{1,0},{4,0},{10,0},{16,0},{17,0},{12,0}};
    srand(time(NULL));
    in = fftw_malloc ( sizeof ( fftw_complex ) * size );
    for(i=0;i<size;i++) {
        for(j=0;j<2;j++) {
            in[i][j] = rand()%size;
        }
    }

    out = fftw_malloc ( sizeof ( fftw_complex ) * size );
    plan_forward = fftw_plan_dft_1d ( n, in, out, FFTW_FORWARD, FFTW_ESTIMATE );
    fftw_execute ( plan_forward );
    sleep(2);    
    return(0);
}
