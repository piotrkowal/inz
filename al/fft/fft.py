import sys
import numpy.fft as np
from time import sleep

def testFft(arg):
    package = 'arg'
    a = getattr(__import__(package, fromlist=['a'+str(arg)]),'a'+str(arg) )
    a = np.fft(a)
    return a


fft = testFft(sys.argv[1])

sleep(10)
