#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
float *conv(float *A, float *B, int lenA, int lenB, int size);
void printData(char *cvar, float *Data, int lenData);

int main(int argc, char **argv)
{
	float *signalA, *signalB, *result;
	int lenA, lenB,size;
	int idx;
	int i;

	sscanf(argv[1],"%i", &lenA);
	sscanf(argv[1],"%i", &lenB);
        size = lenA + lenB - 1; 
	
        signalA = (float*) calloc(lenA, sizeof(float));
	signalB = (float*) calloc(lenB, sizeof(float));
	
	for(i=0; i<size; i++)
	{
		signalA[i] = (float) 1;
                signalB[i] = (float) 1;
	}

	result = conv(signalA, signalB, lenA, lenB, size);

/*	printData("A", signalA, lenA);
	printData("B", signalB, lenB);
        printData("result", result, size);
*/      
        sleep(5);
	return(1);
}

//convolution algorithm
float *conv(float *A, float *B, int lenA, int lenB, int size) {
	int i, j, i1;
	float tmp;
	float *C;

	C = (float*) calloc(size, sizeof(float));

	for (i=0; i<size; i++)
	{
		i1 = i;
		tmp = 0.0;
		for (j=0; j<lenB; j++)
		{
			if(i1>=0 && i1<lenA)
				tmp = tmp + (A[i1]*B[j]);

			i1 = i1-1;
			C[i] = tmp;
		}
	}
	
	return(C);
}

//print data
/*void printData(char *cvar, float *Data, int lenData)
{
	int i;

	printf("Vector '%s' , Size=%i \n", cvar, lenData);
	for(i=0; i<lenData; i++)
		printf("%5.3f \t", Data[i]);

	printf("\n\n");
}*/
